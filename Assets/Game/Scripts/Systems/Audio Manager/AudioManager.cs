﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioManager : Singleton<AudioManager>
{
    public float mBackgroundVolume = 1.0f;
    public float mSfxVolume = 1.0f;


    [System.Serializable]
    public class VolumeChangeEvent : UnityEvent<float> { }

    [HideInInspector] public VolumeChangeEvent onBackgroundVolChange;
    [HideInInspector] public VolumeChangeEvent onSFXVolChange;

    public void SetBackgroundVolume(float pVol)
    {
        ClampVolume(ref pVol);
        mBackgroundVolume = pVol;
        onBackgroundVolChange.Invoke(mBackgroundVolume);
    }

    void ClampVolume(ref float pVol)
    {
        if (pVol > 1.0f)
        {
            pVol = 1.0f;
        }
        else if (pVol < 0.0f)
        {
            pVol = 0.0f;
        }
    }

    public void SetSFXVolume(float pVol)
    {
        ClampVolume(ref pVol);
        mSfxVolume = pVol;
        onSFXVolChange.Invoke(mSfxVolume);
    }

}
