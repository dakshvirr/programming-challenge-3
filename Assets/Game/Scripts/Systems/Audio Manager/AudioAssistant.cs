﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class AudioAssistant : MonoBehaviour
{
    public enum AudioType
    {
        SFX,
        Background
    }

    public AudioType mAudioType;

    public bool isPlaying {
        get { return mAudioSource.isPlaying; }
    }

    AudioSource mAudioSource;

    void Awake()
    {
        mAudioSource = GetComponent<AudioSource>();

        switch(mAudioType)
        {
            case AudioType.Background:
                AudioManager.Instance.onBackgroundVolChange.AddListener(OnVolumeChange);
                mAudioSource.volume = AudioManager.Instance.mBackgroundVolume;
                break;
            case AudioType.SFX:
                AudioManager.Instance.onSFXVolChange.AddListener(OnVolumeChange);
                mAudioSource.volume = AudioManager.Instance.mSfxVolume;
                break;
        }

    }

    void OnDestroy()
    {
        if(!AudioManager.isValidSingleton())
        {
            return;
        }
        switch (mAudioType)
        {
            case AudioType.Background:
                AudioManager.Instance.onBackgroundVolChange.RemoveListener(OnVolumeChange);
                break;
            case AudioType.SFX:
                AudioManager.Instance.onSFXVolChange.RemoveListener(OnVolumeChange);
                break;
        }

    }

    void OnVolumeChange(float pNewVolume)
    {
        mAudioSource.volume = pNewVolume;
    }

    public void PlayOneShot()
    {
        mAudioSource.loop = false;
        mAudioSource.Play();
    }

    public void PlayLoop()
    {
        mAudioSource.loop = true;
        mAudioSource.Play();
    }

    public void Stop()
    {
        mAudioSource.Stop();
    }

    public void ChangeAudio(AudioClip pAudio)
    {
        mAudioSource.clip = pAudio;
    }


}
