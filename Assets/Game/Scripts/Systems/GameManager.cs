﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreUpdateEvent : UnityEvent<int> {}
public class TimeChangeEvent : UnityEvent<string> {}
public class GameManager : Singleton<GameManager>
{
    public SceneReference mMenuScene;
    public SceneReference mGameScene;

    public MenuClassifier mHUDMenu;
    public MenuClassifier mMainMenu;
    public MenuClassifier mWinScreen;
    float mTimeElapsed = 0.0f;
    int mSecondsElapsed = 0;
    int mMinutesElapsed = 0;

    int mCoinsCollected = 0;
    public int mTotalCoins = 0;
    [HideInInspector]
    public bool mGame = false;
    [HideInInspector]
    public ScoreUpdateEvent mScoreUpdatedEvent = new ScoreUpdateEvent();
    [HideInInspector]
    public TimeChangeEvent mTimeChangeEvent = new TimeChangeEvent();
    void Start()
    {
        SceneLoader.Instance.LoadScene(mMenuScene);
        SceneLoader.Instance.onSceneLoadedEvent.AddListener(SceneLoaded);
    }

    void Update()
    {
        if(!mGame)
        {
            return;
        }
        if(mCoinsCollected == mTotalCoins)
        {
            mGame = false;
            mTotalCoins = 0;
            mCoinsCollected = 0;
            SceneLoader.Instance.UnloadScene(mGameScene);
            MenuManager.Instance.ShowMenu(mWinScreen);
        }
        mTimeElapsed += Time.deltaTime;
        int aMin = (int)(mTimeElapsed / 60.0f);
        int aSec = (int)(mTimeElapsed - aMin * 60.0f);
        if(aMin != mMinutesElapsed || aSec != mSecondsElapsed)
        {
            mMinutesElapsed = aMin;
            mSecondsElapsed = aSec;
            string aTimeString = "";
            if(mMinutesElapsed < 10)
            {
                aTimeString += "0";
            }
            aTimeString += mMinutesElapsed.ToString() + ":";
            if(mSecondsElapsed < 10)
            {
                aTimeString += "0";
            }
            aTimeString += mSecondsElapsed.ToString();
            mTimeChangeEvent.Invoke(aTimeString);
        }
    }

    void OnDestroy()
    {
        if(!SceneLoader.isValidSingleton())
        {
            return;
        }
        SceneLoader.Instance.onSceneLoadedEvent.RemoveListener(SceneLoaded);
    }

    public void StartGame()
    {
        SceneLoader.Instance.LoadScene(mGameScene);
    }


    public void CoinCollected()
    {
        mCoinsCollected++;
        mScoreUpdatedEvent.Invoke(mCoinsCollected);
    }

    void SceneLoaded(List<string> pScenes)
    {
        if(pScenes.Contains(mGameScene) && !mGame)
        {
            mGame = true;
            mCoinsCollected = 0;
            mTimeElapsed = 0;
            mSecondsElapsed = 0;
            mMinutesElapsed = 0;
            MenuManager.Instance.ShowMenu(mHUDMenu);
            mScoreUpdatedEvent.Invoke(mCoinsCollected);
            mTimeChangeEvent.Invoke("00:00");
        }
    }


    void OnApplicationQuit()
    {
        SceneLoader.destroy();
        MenuManager.destroy();
        AudioManager.destroy();
        destroy();
    }

}
