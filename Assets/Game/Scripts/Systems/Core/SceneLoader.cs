﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneLoader : Singleton<SceneLoader>
{
	[System.Serializable]
	public class SceneLoadedEvent : UnityEvent<List<string>> { }
	[HideInInspector] public SceneLoadedEvent onSceneLoadedEvent = new SceneLoadedEvent();

	public float delayTime = 1.0f;

	private List<string> loadedScenes = new List<string>();

	public void LoadScene(string scene, bool showLoadingScreen = true)
	{
		StartCoroutine(loadScene(scene, showLoadingScreen, true));
	}

    public void LoadScenes(List<string> scenes, bool showLoadingScreen = true)
    {
        StartCoroutine(loadScenes(scenes, showLoadingScreen, true));
    }

    IEnumerator loadScenes(List<string> scenes, bool showLoadingScreen, bool raiseEvent)
    {
        foreach (string scene in scenes)
        {
            yield return StartCoroutine(loadScene(scene, true, false));
        }
        if(raiseEvent)
        {
            loadedScenes.Clear();
            loadedScenes.AddRange(scenes);
            onSceneLoadedEvent.Invoke(loadedScenes);
        }
    }

    IEnumerator loadScene(string scene, bool showLoadingScreen, bool raiseEvent)
	{
		if (SceneManager.GetSceneByPath(scene).isLoaded)
		{
			if (raiseEvent)
			{
				loadedScenes.Clear();
				loadedScenes.Add(scene);
				onSceneLoadedEvent.Invoke(loadedScenes);
			}
			yield return null;
		}
		else
		{
			if (showLoadingScreen)
			{
				MenuManager.Instance.ShowMenu(MenuManager.Instance.mLoadingMenu);
			}

			yield return new WaitForSeconds(delayTime);

            Application.backgroundLoadingPriority = ThreadPriority.Low;

			AsyncOperation sync;

			sync = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
			while (sync.isDone == false) { yield return null; }

            Application.backgroundLoadingPriority = ThreadPriority.Normal;

            yield return new WaitForSeconds(delayTime);

			if (raiseEvent)
			{
				loadedScenes.Clear();
				loadedScenes.Add(scene);
				onSceneLoadedEvent.Invoke(loadedScenes);
			}

			if (showLoadingScreen)
			{
				MenuManager.Instance.HideMenu(MenuManager.Instance.mLoadingMenu);
			}
		}


	}


    public void UnloadScene(string scene)
    {
        StartCoroutine(unloadScene(scene));
    }

    IEnumerator unloadScene(string scene)
    {
        AsyncOperation aSync = null;
        try
        {
            aSync = SceneManager.UnloadSceneAsync(scene);

        }
        catch(System.Exception aE)
        {
            Debug.Log(aE.Message);
        }

        if (aSync != null)
        {
            yield return new WaitUntil(() => aSync.isDone);
        }

        aSync = Resources.UnloadUnusedAssets();
        yield return new WaitUntil(() => aSync.isDone);
    }

    public void UnloadScenes(List<string> scenes)
    {
        StartCoroutine(unloadScenes(scenes));
    }

    IEnumerator unloadScenes(List<string> scenes)
    {
        foreach(string scene in scenes)
        {
            yield return StartCoroutine(unloadScene(scene));
        }
    }


}
