﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
	public MenuClassifier mMenuClassifier;
	public enum StartMenuState
	{
		Ingnore,
		Active,
		Disabled
	}
	public StartMenuState mStartMenuState = StartMenuState.Active;
	public bool mResetPosition = true;

	public virtual void Awake()
	{
		MenuManager.Instance.AddMenu(this);
		if (mResetPosition == true)
		{
			var rect = GetComponent<RectTransform>();
			rect.localPosition = Vector3.zero;
		}
	}

	public virtual void Start()
	{
		switch(mStartMenuState)
		{
			case StartMenuState.Active:
				gameObject.SetActive(true);
				break;

			case StartMenuState.Disabled:
				gameObject.SetActive(false);
				break;
		}
	}

	public virtual void ShowMenu(string pOptions)
	{
        gameObject.SetActive(true);
	}

	public virtual void HideMenu(string pOptions)
	{
        gameObject.SetActive(false);
	}

}
