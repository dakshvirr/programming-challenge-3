﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(BoxCollider2D))]
public class Trap : MonoBehaviour
{
    bool mTrapActive = false;
    public float mTrapFireTime = 10.0f;
    public GameObject mFireAnimation;
    float mCurTimer = 0.0f;

    public AudioAssistant mFire;
    public AudioAssistant mTrap;


    void OnTriggerEnter2D(Collider2D collision)
    {
        if(mTrapActive)
        {
            if(mFireAnimation.activeInHierarchy)
            {
                Player aPlayer = collision.gameObject.GetComponentInParent<Player>();
                if(aPlayer == null)
                {
                    return;
                }
                if(!aPlayer.mDead)
                {
                    aPlayer.KillPlayer();
                }
            }
            return;
        }
        if(collision.gameObject.GetComponentInParent<Player>())
        {
            mTrapActive = true;
            mTrap.PlayOneShot();
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if(mTrapActive && mFireAnimation.activeInHierarchy)
        {
            Player aPlayer = collision.gameObject.GetComponentInParent<Player>();
            if (aPlayer == null)
            {
                return;
            }
            if (!aPlayer.mDead)
            {
                aPlayer.KillPlayer();
            }
        }
    }

    void Update()
    {
        if(!mTrapActive)
        {
            return;
        }
        if(mFireAnimation.activeInHierarchy)
        {
            return;
        }
        mCurTimer += Time.deltaTime;
        if(mCurTimer >= mTrapFireTime)
        {
            mFireAnimation.SetActive(true);
            mFire.PlayOneShot();
        }
    }
}