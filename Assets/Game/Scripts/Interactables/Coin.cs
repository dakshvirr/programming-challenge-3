﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioAssistant))]
public class Coin : MonoBehaviour
{
    AudioAssistant mAudio;
    SpriteRenderer mRenderer;
    bool mDisabled = false;

    void Start()
    {
        GameManager.Instance.mTotalCoins++;
        mAudio = GetComponent<AudioAssistant>();
        mRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(mDisabled && !mAudio.isPlaying)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(mDisabled)
        {
            return;
        }
        if (collision.gameObject.GetComponentInParent<Player>())
        {
            mAudio.PlayOneShot();
            mRenderer.enabled = false;
            GameManager.Instance.CoinCollected();
            mDisabled = true;
        }
    }
}
