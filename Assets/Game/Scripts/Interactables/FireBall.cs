﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    [HideInInspector]
    public Vector2 mMoveDir;
    public float mSpeed;

    void Update()
    {
        transform.position += (Vector3)( mSpeed * Time.deltaTime * mMoveDir);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag != "Wall")
        {
            return;
        }
        Destroy(gameObject);
    }

}
