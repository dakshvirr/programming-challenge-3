﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : Menu
{
    public AudioAssistant mButtonSound;

    public void StartGame()
    {
        mButtonSound.PlayOneShot();
        GameManager.Instance.StartGame();
        MenuManager.Instance.HideMenu(mMenuClassifier);
    }
}
