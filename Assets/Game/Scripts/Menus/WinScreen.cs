﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScreen : Menu
{
    public AudioAssistant mAudio;

    public void PlayAgain()
    {
        mAudio.PlayOneShot();
        GameManager.Instance.StartGame();
        MenuManager.Instance.HideMenu(mMenuClassifier);
    }
}
