﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class HUD : Menu
{
    public TextMeshProUGUI mTimer;
    public TextMeshProUGUI mScore;

    void OnEnable()
    {
        GameManager.Instance.mScoreUpdatedEvent.AddListener(ScoreUpdate);
        GameManager.Instance.mTimeChangeEvent.AddListener(TimeUpdate);
    }

    void OnDisable()
    {
        if(!GameManager.isValidSingleton())
        {
            return;
        }
        GameManager.Instance.mScoreUpdatedEvent.RemoveListener(ScoreUpdate);
        GameManager.Instance.mTimeChangeEvent.RemoveListener(TimeUpdate);
    }

    void ScoreUpdate(int pScore)
    {
        mScore.text = pScore.ToString();
    }

    void TimeUpdate(string pTimeString)
    {
        mTimer.text = pTimeString;
    }

}
