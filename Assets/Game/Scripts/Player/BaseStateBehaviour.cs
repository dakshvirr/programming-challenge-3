﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStateBehaviour : StateMachineBehaviour
{
    protected Player mPlayer;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(mPlayer == null)
        {
            mPlayer = animator.gameObject.GetComponentInParent<Player>();
        }
    }

}
