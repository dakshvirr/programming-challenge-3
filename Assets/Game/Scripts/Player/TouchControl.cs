﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControl : MonoBehaviour
{
    [System.Serializable]
    public struct SwipeTouch
    {
        public Vector2 mInitTouchPos;
        public float mCurrentTouchTimer;
    }
    [SerializeField] Player mActivePlayer;
    [SerializeField] SpriteRenderer mSelfBounds;
    [SerializeField] GameObject mControlObject;
    [SerializeField] float mMaxSwipeTime = 0.3f;
    [SerializeField] float mMinimumSwipeDistance = 3.0f;
    int mMovementFingerId = -1;
    readonly Dictionary<int, SwipeTouch> mSwipeTouches = new Dictionary<int, SwipeTouch>();
    void Start()
    {
        Input.multiTouchEnabled = true;
    }

    void Update()
    {
        MovementUpdate();
        SwipeCheckUpdate();
    }

    void SwipeCheckUpdate()
    {
        List<int> aFingerRemoval = new List<int>();
        List<int> aFingerIds = new List<int>(mSwipeTouches.Keys);
        foreach(int aFingerId in aFingerIds)
        {
            SwipeTouch aSwipeTouch = mSwipeTouches[aFingerId];
            aSwipeTouch.mCurrentTouchTimer += Time.deltaTime;
            if(aSwipeTouch.mCurrentTouchTimer >= mMaxSwipeTime)
            {
                aFingerRemoval.Add(aFingerId);
            }
            mSwipeTouches[aFingerId] = aSwipeTouch;
        }
        foreach(int aFingerId in aFingerRemoval)
        {
            mSwipeTouches.Remove(aFingerId);
        }
        if(Input.touchCount > 0)
        {
            foreach(Touch aTouch in Input.touches)
            {
                if(mMovementFingerId == aTouch.fingerId)
                {
                    continue;
                }
                if (aTouch.phase == TouchPhase.Began)
                {
                    SwipeTouch aSwipeTouch = new SwipeTouch();
                    aSwipeTouch.mCurrentTouchTimer = 0.0f;
                    aSwipeTouch.mInitTouchPos = aTouch.position;
                    mSwipeTouches.Add(aTouch.fingerId, aSwipeTouch);
                }
                else if (mSwipeTouches.ContainsKey(aTouch.fingerId))
                {
                    if (aTouch.phase == TouchPhase.Ended)
                    {
                        SwipeTouch aSwipeTouch = mSwipeTouches[aTouch.fingerId];
                        Vector3 aWorldSwipePos = Camera.main.ScreenToWorldPoint(aSwipeTouch.mInitTouchPos);
                        aWorldSwipePos.z = 0;
                        Vector3 aWorldEndPos = Camera.main.ScreenToWorldPoint(aTouch.position);
                        aWorldEndPos.z = 0;
                        Vector3 aDistanceVector = aWorldEndPos - aWorldSwipePos;
                        Debug.Log(aDistanceVector.magnitude);
                        if(aDistanceVector.magnitude >= mMinimumSwipeDistance)
                        {
                            mActivePlayer.Fire(aDistanceVector.normalized);
                        }
                        mSwipeTouches.Remove(aTouch.fingerId);
                    }
                    else if(aTouch.phase == TouchPhase.Canceled)
                    {
                        mSwipeTouches.Remove(aTouch.fingerId);
                    }
                }
            }
        }
    }

    void MovementUpdate()
    {
        if(Input.touchCount > 0)
        {
            foreach(Touch aTouch in Input.touches)
            {
                if(mMovementFingerId == -1)
                {
                    if(aTouch.phase == TouchPhase.Began)
                    {
                        Vector3 aWorldPos = Camera.main.ScreenToWorldPoint(aTouch.position);
                        aWorldPos.z = 0;
                        if(mSelfBounds.bounds.Contains(aWorldPos))
                        {
                            mMovementFingerId = aTouch.fingerId;
                            HandleMoveTouch(aTouch);
                        }
                    }
                }
                else
                {
                    if(aTouch.fingerId != mMovementFingerId)
                    {
                        continue;
                    }
                    HandleMoveTouch(aTouch);
                }
            }
        }
    }

    void HandleMoveTouch(Touch pTouch)
    {
        if(pTouch.phase == TouchPhase.Ended || pTouch.phase == TouchPhase.Canceled)
        {
            mMovementFingerId = -1;
            mActivePlayer.MovePlayer(Vector2.zero);
            mControlObject.transform.position = mSelfBounds.bounds.center;
            return;
        }
        Vector3 aTouchWorldPosition = Camera.main.ScreenToWorldPoint(pTouch.position);
        aTouchWorldPosition.z = 0;
        aTouchWorldPosition = mSelfBounds.bounds.ClosestPoint(aTouchWorldPosition);
        Vector3 aMoveDist = aTouchWorldPosition - mSelfBounds.bounds.center;
        if(aMoveDist.magnitude > mSelfBounds.bounds.extents.x)
        {
            aTouchWorldPosition = mSelfBounds.bounds.center + aMoveDist.normalized * mSelfBounds.bounds.extents.x;
        }
        mControlObject.transform.position = aTouchWorldPosition;
        mActivePlayer.MovePlayer(aMoveDist.normalized);
    }
}
