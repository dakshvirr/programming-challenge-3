﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(AudioAssistant))]
public class Player : MonoBehaviour
{
    [HideInInspector]
    public bool mDead;
    public float mSpeed;
	public float mMovementThreshold = 0.3f;
    public AudioClip mAttackSound;
    public AudioClip mDeathSound;

    AudioAssistant mAudio;
    Animator mAnimator;
    Rigidbody2D mRigidBody;
    Vector2 mMovementVector;
    Vector2 mFireDirVector;


    public GameObject mFireballPrefab;

    Vector3 mSpawnPos;

    void Start()
    {
        mAnimator = GetComponent<Animator>();
        mRigidBody = GetComponent<Rigidbody2D>();
        mAudio = GetComponent<AudioAssistant>();
        mSpawnPos = transform.position;
    }

    void Update()
    {
#if UNITY_EDITOR
        OldUpdate();
#endif
    }

    public void MovePlayer(Vector2 pMovementVector)
    {
        if (mDead)
        {
            mMovementVector = Vector2.zero;
            return;
        }
        mMovementVector = pMovementVector;
        SetAnimatorValues();
    }

    public void Fire(Vector2 pFireDirVector)
    {
        if(mDead)
        {
            return;
        }
        mFireDirVector = pFireDirVector;
        mAnimator.SetTrigger("Attack");
    }

    void OldUpdate()
    {
        if (mDead)
        {
            mMovementVector = Vector2.zero;
            return;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            mAnimator.SetTrigger("Attack");
        }
        mMovementVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        SetAnimatorValues();
        mFireDirVector = new Vector2(mAnimator.GetFloat("Horizontal"), mAnimator.GetFloat("Vertical"));
    }

    void SetAnimatorValues()
    {
        float aMagX = Mathf.Abs(mMovementVector.x);
        float aMagY = Mathf.Abs(mMovementVector.y);
        float aIdleMag = aMagX > aMagY ? aMagX : aMagY;
        mAnimator.SetFloat("IdleMag", aIdleMag);
        if (aIdleMag > mMovementThreshold)
        {
            mAnimator.SetFloat("Horizontal", mMovementVector.x);
            mAnimator.SetFloat("Vertical", mMovementVector.y);
        }
        else
        {
            mMovementVector = Vector2.zero;
        }
    }

    void FixedUpdate()
    {
        mRigidBody.velocity = mMovementVector * mSpeed;
    }


    public void KillPlayer()
    {
        mDead = true;
        mAudio.ChangeAudio(mDeathSound);
        mAudio.PlayOneShot();
        mAnimator.SetTrigger("Die");
    }

    public void Attack()
    {
        GameObject aFireball = Instantiate(mFireballPrefab, transform.position, 
            Quaternion.AngleAxis(Mathf.Atan2(mFireDirVector.x, -mFireDirVector.y) * Mathf.Rad2Deg,Vector3.forward));
        FireBall aFireballInst = aFireball.GetComponent<FireBall>();
        aFireballInst.mMoveDir = mFireDirVector;
        if(aFireballInst.mMoveDir == Vector2.zero)
        {
            aFireballInst.mMoveDir = Vector2.down;
        }
        mAudio.ChangeAudio(mAttackSound);
        mAudio.PlayOneShot();
        aFireball.SetActive(true);
    }

    public void ReSpawn()
    {
        gameObject.SetActive(false);
        transform.position = mSpawnPos;
        mDead = false;
        gameObject.SetActive(true);
    }

}
